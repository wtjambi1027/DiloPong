﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Pemain 1
    public PlayerControl player1; // script
    private Rigidbody2D player1RigidBody;

    // Pemain 2
    public PlayerControl player2; // script
    private Rigidbody2D player2RigidBody;

    // Bola
    public BallControl ball; // script
    private Rigidbody2D ballRigidBody;
    private CircleCollider2D ballCollider;

    // Skor maksimal
    public int maxScore;

    // Apakah debug window ditampilkan?
    private bool isDebugWindowShown = false;

    // Objek untuk menggambarkan lintasan bola
    public Trajectory trajectory;

    // Start is called before the first frame update
    void Start()
    {
        // Inisialisasi rigidbody dan collider
        player1RigidBody = player1.GetComponent<Rigidbody2D>();
        player2RigidBody = player2.GetComponent<Rigidbody2D>();
        ballRigidBody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    // Untuk menampilkan GUI
    void OnGUI()
    {
        // Membuat dan menampilkan skor player 1 di kiri atas, dan skor player 2 di kanan atas
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.Score);

        // ======================================================================================================================
        // Saya menemukan bug dimana pemain dapat menekan tombol restart lebih dari sekali,
        // yang dimana akan ada "queue of restarts"
        // (pemahaman saya belum terlalu dalam terkait cara kerja function Invoke() dan delay-nya).
        // Namun, dari estimasi Saya, bug ini bisa dengan mudah diperbaiki dengan,
        // memastikan bola sedang bergerak, untuk tombol restart bisa bekerja
        // sebagai berikut, 
        // if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART") && ballRigidBody.velocity != Vector2.zero)
        // { LakukanRestart(); }
        // ======================================================================================================================

        // Membuat tombol restart untuk memulai ulang game dari awal
        // Jika tombol ditekan...
        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART"))
        {
            // ...reset skor kedua pemain
            player1.ResetScore();
            player2.ResetScore();

            // ...restart game
            ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }

        // Jika pemain 1 mencapai skor maksimal...
        if (player1.Score == maxScore)
        {
            // ...tampilkan teks "PLAYER ONE WINS" di bagian kiri layar
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER ONE WINS");

            // ...kemabalikan bola ke tengah
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        // Sebaliknya, jika pemain 1 mencapai skor maksimal...
        else if (player2.Score == maxScore)
        {
            // ...tampilkan teks "PLAYER TWO WINS" di bagian kiri layar
            GUI.Label(new Rect(Screen.width / 2 + 30, Screen.height / 2 - 10, 2000, 1000), "PLAYER TWO WINS");

            // ...kemabalikan bola ke tengah
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        // Jika isDebugWindowShown = true, tampilkan text area debug window
        if (isDebugWindowShown)
        {
            // Nilai warna awal background GUI
            Color oldColor = GUI.backgroundColor;

            // Beri warna baru background GUI
            GUI.backgroundColor = Color.red;

            // Mereference info - info fisika yang akan ditampilkan
            float ballMass = ballRigidBody.mass;
            Vector2 ballVelocity = ballRigidBody.velocity;
            float ballSpeed = ballRigidBody.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            string debugText =
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" +
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";

            // Tampilkan debug window
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 100), debugText, guiStyle);

            // Kembalikan warna lama GUI
            GUI.backgroundColor = oldColor;
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
        }
    }
}

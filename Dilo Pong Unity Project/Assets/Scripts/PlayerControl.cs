﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Tombol untuk menggerakkan ke atas
    public KeyCode upButton = KeyCode.W;

    // Tombol untuk menggerakkan ke bawah
    public KeyCode downButton = KeyCode.S;

    // Kecepatan gerak
    public float speed = 10.0f;

    // Batas atas dan bawah game scence (batas bawah dikali minus 1/ dijadikan minus)
    public float yBoundary = 9.0f;

    // Component RigidBody2D objek ini
    private Rigidbody2D rigidBody2D;

    // Skor pemain ini
    private int score;


    // Untuk mengetahui informasi terkait collision dengan bola
    private ContactPoint2D lastContactPoint;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Mendapatkan kecepatan raket pada frame ini
        Vector2 velocity = rigidBody2D.velocity;

        // Jika pemain menekan tombol ke atas, nilai kecepatan player menjadi speed (positif, ke atas)
        if (Input.GetKey(upButton))
        {
            velocity.y = speed;
        }

        // Jika pemain menekan tombol ke bawah, nilai kecepatan player menjadi -speed (negatif, ke bawah)
        else if (Input.GetKey(downButton))
        {
            velocity.y = -speed;
        }

        // Jika pemain tidak menekan kedua tombol di atas, kecepatan menjadi nol
        else
        {
            velocity.y = 0;
        }

        // Meng-assign nilai var velocity kepada velocity rigidBody2D
        rigidBody2D.velocity = velocity;

        // Mendapatkan posisi raket pada frame ini
        Vector3 position = transform.position;

        // Jika posisi y melebihi batas atas (yBoundary), kembalikan nilai posisi y ke batas atas
        if (position.y > yBoundary)
        {
            position.y = yBoundary;
        }

        // Jika posisi y kurang dari batas bawah (-yBoundary), kembalikan nilai posisi y ke batas bawah 
        else if (position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        // Meng-assign nilai position ke transform.position
        transform.position = position;
    }

    // Ketika terjadi collision dengan bola, rekam titik kontaknya
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            lastContactPoint = collision.GetContact(0);
        }
    }

    // Menaikkan skor sebanyak 1 poin
    public void IncrementScore()
    {
        score++;
    }

    // Mengembalikan nilai skor menjadi nol
    public void ResetScore()
    {
        score = 0;
    }

    // Properti Score, untuk mengakses skor dari kelas lain
    public int Score
    {
        get { return score; }
    }

    // Untuk kelas lain yang menginginkan informasi kontak dengan player
    public ContactPoint2D LastContactPoint
    {
        get { return lastContactPoint; }
    }
}

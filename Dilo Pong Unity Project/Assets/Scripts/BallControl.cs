﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    // RigidBody2D Bola
    private Rigidbody2D rigidBody2D;

    // Gaya awal yang mendorong bola
    public float xInitialForce;
    public float yInitialForce;

    // Titik asal lintasan saat ini
    private Vector2 trajectoryOrigin;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();

        trajectoryOrigin = transform.position;

        // Mulai game
        RestartGame();
    }

    // Ketika bola beranjak dari sebuah tumbukan, rekam titik tumbukan tersebut
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    void ResetBall()
    {
        // Reset posisi menjadi (0, 0)
        transform.position = Vector2.zero;

        // Reset kecepatan menjadi (0, 0)
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        // ============================================================================================================
        // Beberapa bagian dalam function ini yang diubah dari tutorial untuk assignment, yaitu : 
        // 1.   Berhenti menggunakan yRandomInitialForce
        //      - bagian yRandomInitialForce dijadikan comment
        // 2.   Menentukan arah y dengan cara yang sama dengan x, tidak lagi dengan yRandomInitialForce
        //      - karena tetap ingin arah random, tapi initial force-nya tidak random
        //      - kemungkinan arah menjadi : kiri atas, kanan atas, kiri bawah, kanan bawah
        //      - menggunakan xRandomDirection dan yRandomDirection
        // ============================================================================================================

        // Menentukan nilai awal gaya dorong sumbu y, antara -yInitialForce dan yInitialForce
        // float yRandomInitalForce = Random.Range(-yInitialForce, yInitialForce);

        // Menentukan nilai acak antar 0 (inklusif) dan 2 (ekslusif), untuk arah x dan arah y
        float xRandomDirection = Random.Range(0, 2);
        float yRandomDirection = Random.Range(0, 2);

        // Jika randomDirection < 1, gaya awal ke kiri, selainnya ke kanan
        if (xRandomDirection < 1.0f)
        {
            if (yRandomDirection < 1.0f)
                rigidBody2D.AddForce(new Vector2(-xInitialForce, -yInitialForce));
            else
                rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));
        }
        else
        {
            if (yRandomDirection < 1.0f)
                rigidBody2D.AddForce(new Vector2(xInitialForce, -yInitialForce));
            else
                rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));
        }
    }

    void RestartGame()
    {
        // Kembalikan bola ke posisi semula
        ResetBall();

        // Setelah 2 detik, berikan gaya ke bola
        Invoke("PushBall", 2);
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin;  }
    }

}

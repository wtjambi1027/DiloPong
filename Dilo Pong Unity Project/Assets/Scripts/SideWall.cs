﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{
    // Mengakses script pemain, untuk menambahkan skor ketika bola menyentuh dinding ini
    public PlayerControl player;

    // Skrip GameManager untuk mengakses skor maksimal
    [SerializeField]
    private GameManager gameManager;

    // Akan dipanggil ketika objek ber-collider lain menyentuh dinding ini
    private void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        // Jika anotherCollider bernama "Ball"
        if (anotherCollider.name == "Ball")
        {
            // Tambahkan 1 skor ke pemain
            player.IncrementScore();

            // Jika skor pemain dibawah skor maksimal...
            if (player.Score < gameManager.maxScore)
            {
                // ...restart game setelah 2 detik
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}

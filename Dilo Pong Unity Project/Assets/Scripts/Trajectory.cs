﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    // Script, collider, dan rigidbody bola
    public BallControl ball;
    CircleCollider2D ballCollider;
    Rigidbody2D ballRigidBody;

    // Bola "bayangan" yang akan ditampilkan pada titik tumbukan
    public GameObject ballAtCollision;

    // Start is called before the first frame update
    void Start()
    {
        // Inisialisasi ballCollider dan ballRidigBody
        ballCollider = ball.GetComponent<CircleCollider2D>();
        ballRigidBody = ball.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Flag yang menentukan ditampakkannya bola bayangan jika pantulan raycast bertumbuk
        bool drawBallAtCollision = false;

        // Titik tumbuk yang digeser, untuk menggambar ballAtCollision dari tengah
        Vector2 offsetHitPoint = new Vector2();

        // Tentukan titik tumbukan dengan deteksi pergerakan lingkaran
        RaycastHit2D[] circleCastHit2DArray = Physics2D.CircleCastAll(ballRigidBody.position, ballCollider.radius, ballRigidBody.velocity.normalized);

        // Untuk setiap tumbukan...
        foreach (RaycastHit2D circleCastHit2D in circleCastHit2DArray)
        {
            // Jika terjadi tumbukan, mengecualikan bola itu sendiri (karena raycast dimulai dari titik tengah bola)
            if (circleCastHit2D.collider != null &&
                circleCastHit2D.collider.GetComponent<BallControl>() == null)
            {
                /*
                 * Garis lintasan terbentuk dari titik sekarang, ke titik terjadinya tumbukan,
                 * yang didapat dari titik tumbukan yang di-offset atau geser berdasar vektor normal di titik tumbukan,
                 * sebesar jari - jari bola (dari pinggir ke tengah) 
                */

                // Tentukan titik tumbukan
                Vector2 hitPoint = circleCastHit2D.point;

                // Tentukan normal di titik tumbukan
                Vector2 hitNormal = circleCastHit2D.normal;

                // Tentukan offsetHitpoint, yaitu titik tengah bola saat tumbukan terjadi
                offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;

                // Gambar garis lintasan, dari titik tengah bola pada frame ini, ke titik tengah bola saat tumbukan terjadi
                DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);

                // Kalau objek yang bertumbuk dengan bola bukan SideWall, gambar pantulannya
                if (circleCastHit2D.collider.GetComponent<SideWall>() == null)
                {
                    // Hitung vektor datang
                    Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;

                    // Hitung vektor keluar
                    Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                    // Hitung dot product dari outVector dan hitNormal, digunakan supaya garis lintasan ketika
                    // terjadi tumbukan, tidak digambar
                    float outDot = Vector2.Dot(outVector, hitNormal);
                    if (outDot > -1.0f && outDot < 1.0f)
                    {
                        // Gambar garis lintasan pantulan
                        DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint, offsetHitPoint + outVector * 10.0f);

                        // Gambar bola "bayangan" di titik tumbukan dari hasil prediksi
                        drawBallAtCollision = true;
                    }
                }
                // Hanya gambar lintasan untuk satu titik tumbukan, break untuk keluar dari loop
                break;
            }
        }

        if (drawBallAtCollision)
        {
            // Gambar bola "bayangan" di titik tumbukan yang di prediksikan
            ballAtCollision.transform.position = offsetHitPoint;
            ballAtCollision.SetActive(true);
        }
        else
        {
            // Sembunyikan bola "bayangan"
            ballAtCollision.SetActive(false);
        }
    }
}
